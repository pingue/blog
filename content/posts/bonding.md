+++
title = "Bonding"
date = "2013-12-18T00:00:00+00:00"
tags = ["network"]
categories = ["linux"]
banner = "img/avatar.png"
+++

See https://www.kernel.org/doc/Documentation/networking/bonding.txt

For the slave interfaces, set each of their network configs as such, noting the last 2 lines:

```
DEVICE="eth0"
HWADDR="xx:xx:xx:xx:xx:xx"
ONBOOT="yes"
TYPE="Ethernet"
UUID="xxxxxx"
NM_CONTROLLED=no
SLAVE=yes
MASTER=bond0
```

And the master as such

```
DEVICE="bond0"
IPV6INIT="no"
MTU="1500"
ONBOOT="yes"
NM_CONTROLLED=no
BOOTPROTO=static
IPADDR=xxx.xxx.xxx.xxx
NETMASK=xxx.xxx.xxx.xxx
GATEWAY=xxx.xxx.xxx.xxx
BONDING_OPTS="mode=1 miimon=100"
```

The bonding config option `mode=1` enables Active-Standby mode

The bonding config option `miimon=100` polls the interfaces every 100ms too see if one has gone down and to fail over if necessary.

The documentation says:

```
miimon

    Specifies the MII link monitoring frequency in milliseconds.
    This determines how often the link state of each slave is
    inspected for link failures.  A value of zero disables MII
    link monitoring.  A value of 100 is a good starting point.
    The use_carrier option, below, affects how the link state is
    determined.  See the High Availability section for additional
    information.  The default value is 0.
```

Therefore, by default, if one interface goes down, the bonded interface won't realise and will continue to try sing it!

See also: http://www.linux-corner.info/bonding.html


