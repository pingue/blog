+++
title = "VMFS Recovery"
date = "2020-11-01T10:00:00+00:00"
tags = ["vmware"]
categories = ["tech"]
banner = "img/avatar.png"
+++

# VMFS recovery

```
vmfs-fuse /dev/sdc1 /mnt/vmfs

losetup --partscan --find --show /mnt/vmfs/VM/VM-flat.vmdk

pvscan --cache /dev/loop0p2

vgchange -ay vg_vmname

fsck -y /dev/mapper/vg_vmname_lv_root

mount /dev/mapper/vg_vmname_lv_root /mnt/VM
```

