+++
title = "Permissions Tree"
date = "2013-12-18T00:00:00+00:00"
tags = ["filesystem"]
categories = ["linux"]
banner = "img/avatar.png"
+++

View the permissions of all files and directories above a path

**Mode**
```
$> namei -m /full/pathname
f: /full/pathname
 dr-xr-xr-x /
 drwxr-xr-x full
 drwxr-xr-x pathname
```

**Owner/Group**
```
$> namei -o /full/pathname
f: /full/pathname
 d root root /
 d root root full
 d root root pathname
```

**All**
```
$> namei -l /full/pathname
f: /full/pathname
dr-xr-xr-x root root /
drwxr-xr-x root root full
drwxr-xr-x root root pathname
```
