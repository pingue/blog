+++
title = "Service Dead but Subsystem Locked"
date = "2016-02-03T00:00:00+00:00"
tags = ["services"]
categories = ["linux"]
banner = "img/avatar.png"
+++

```
# service php-fpm status
php-fpm dead but subsys locked
```

This will also cause issues when trying to stop a service - it will fail and the processes will not be killed off

This is caused when the PID file and the lock file do not match up. They must have the same name. Note the folder being "php6-fpm" rather than "php-fpm"

```
# ls -l /var/run/php6-fpm
total 4
-rw-r--r-- 1 root   root   4 Feb 23 14:22 php-fpm.pid
```

```
# ls -l /var/lock/subsys/php*
-rw-r--r-- 1 root root 0 Feb 23 14:22 /var/lock/subsys/php-fpm
```

In this instance, the fix was to alter the path to the pid file, which had been manually set.


http://serverfault.com/questions/26276/what-does-service-name-dead-but-subsys-locked-mean
