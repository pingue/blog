+++
title = "Deleted File Recovery on Linux"
date = "2013-12-18T00:00:00+00:00"
tags = ["filesystem"]
categories = ["linux"]
banner = "img/avatar.png"
+++

The following obviously only works if file handle is still open (`lsof` will tell you this...). Note:

**ONLY DO THIS IF YOU UNDERSTAND WHAT YOU'RE DOING**

Firstly, you need an inode number (again, use `lsof`)

Run:

```
debugfs -w /dev/mapper/sysvg-home -R 'link <134601> /mike/foo'
```

where:

```
/dev/mapper/sysvg-home = filesystem 
134601 = inode #
/mike/foo = path relative to filesystem (ie you want to recover /home/mike/foo )
```

See http://dag.wieers.com/blog/undeleting-an-open-file-by-inode

This works by relinking the path to its inode at a filesystem level. Sounds dirty, but it works!


