+++
title = "Extract Single Table from MySQL Dump"
date = "2015-04-23T00:00:00+00:00"
tags = ["mysql"]
categories = ["mysql"]
banner = "img/avatar.png"
+++

See http://blog.tsheets.com/2008/tips-tricks/extract-a-single-table-from-a-mysqldump-file.html

Alternatively: https://github.com/abg/dbsake
