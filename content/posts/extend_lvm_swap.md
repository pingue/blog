+++
title = "Extend LVM Swap"
date = "2014-01-28T00:00:00+00:00"
tags = ["filesystem"]
categories = ["linux"]
banner = "img/avatar.png"
+++

See also http://pubmem.wordpress.com/2010/09/16/how-to-resize-lvm-logical-volumes-with-ext4-as-filesystem/ and https://access.redhat.com/site/documentation/en-US/Red_Hat_Enterprise_Linux/5/html/Deployment_Guide/s1-swap-adding.html

```
umount /data
e2fsck -f /dev/mapper/sysvg-data #Should be fine
resize2fs -p /dev/mapper/sysvg-data 240G #8Gb smaller
lvreduce -L 240G /dev/mapper/sysvg-data
e2fsck -f /dev/mapper/sysvg-data #Might bail out. Don't panic, see linked documents.
mount /data
swapoff -a
lvresize -l +100%FREE /dev/mapper/sysvg-swap 
mkswap /dev/mapper/sysvg-swap
swapon -a
```
