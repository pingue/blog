+++
title = "Showmaster Lighting Desk Cheatsheet"
date = "2019-11-06T23:35:46+00:00"
tags = ["lighting"]
categories = ["tech"]
banner = "img/avatar.png"
+++

## Desk

The showtec showmaster 24 / 48 is a lighting desk suited to live music events

### Notes

* The desk starts in blackout mode (the blackout light rapidly flashes). Press the blackout button to cancel this.

* The default state is to be in "Single Chase" mode. This prevents multiple cue stacks to be active simultaneously. Possibly useful in a live music setting, but "Mix Chase" mode is more suitable for most functions.

* The A stack is full with the fader up. The B stack is full with the fader down.

* For use as a set of "subs", Fade should be min (up), Speed should be max (down), and audio level should be off (down)

### Recording states

1. Hold the REC button, and press the flash buttons 1, 6, 6, 8

1. Select the relevant mode (probably "single")

1. Set the faders to the relevant state

1. Press "REC". This stores the current state into temporary memory

1. The previous 2 steps can be repeated to create a chase, rather than a cue

1. Once complete, hold the REC button and press the relevant flash button (bottom row). If multiple cues were in memory, a chase is created, otherwise a sub is created.

1. Press "REC" plus "REC/EXIT" to return to normal mode

1. Select "Chase/Scenes" mode to use the stored stack on the bottom row

### Factory Reset

* Hold the REC button, and press the flash buttons 1, 6, 6, 8

* Hold the REC button, and press the flash buttons 1, 4, 2, 3


### References

* https://assets.highlite.com/attachments/MANUAL/50831_MANUAL_GB_V1.pdf


